#include <stdio.h>
#include <iostream>
#include <string.h>
using namespace std;
void search(char *pat, char *txt)
{
int M = strlen(pat);
int N = strlen(txt);
/* A loop to slide pat[] one by one */
for (int i = 0; i <= N - M; i++)
{
int j;
/* For current index i, check for pattern match */
for (j = 0; j < M; j++)
{
if (pat[j] != '*' && txt[i + j] != pat[j])
break;
}
if (j == M) // if pat[0...M-1] = txt[i, i+1, ...i+M-1]
{
cout << "Pattern found at index " << i << "\n";
}
}
}
/* Driver program to test above function */
int main()
{
char txt[20];
char pat[20];
cout << "Enter text: ";
cin >> txt;
cout << "Enter pattern: ";
cin >> pat;
search(pat, txt);
return 0;
}
